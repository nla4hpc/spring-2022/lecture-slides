This repository stores the slides for the [NLA4HPC lecture](https://www.math.kit.edu/ianm4/edu/numlinalg2022s/).

Besides the lecture slides, here are some additional

- https://ppc.cs.aalto.fi/ a lecture course focused on high performance optimizations using OpenMP and CUDA

#### OpenMP
- https://www.openmp.org/specifications/ contains the whole openMP specification, a short reference sheet, and examples. Please choose the 4.5 specification as newer one might not be completely supported by all compilers.

#### CUDA
- https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html contains the CUDA programming guide
- https://docs.nvidia.com/cuda/cuda-c-best-practices-guide/index.html has some tips on optimizing your program
- https://docs.nvidia.com/nsight-compute/ is the documentation for the NVIDIA profiling
- https://developer.nvidia.com/blog/using-nsight-compute-to-inspect-your-kernels/ an introduction to profiling with nsight compute
- https://developer.nvidia.com/blog/even-easier-introduction-cuda/ an introduction to cuda with many references listed
- https://developer.nvidia.com/blog/analysis-driven-optimization-preparing-for-analysis-with-nvidia-nsight-compute-part-1/ three part series on optimization guided by nsight compute
